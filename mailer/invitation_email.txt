{% spaceless %}
{% load i18n %}
{% blocktrans with protocol=protocol site_domain=site_domain app_name=app_name app_manage=app_manage token=token email=email list_description=list_description %}[{{ app_name }}] Invited you to a private list [{{list_description}}]

Should you cancel your subscription to a private list,
you will not be able to resubscribe it unless invited again.

To receive any messages from this list, you have to confirm your will first.

Here is a link for your subscription management and will confirmation:
{{ protocol }}://{{ site_domain }}{{ app_manage }}?token={{ token }}
This link has no expiration date and should be valid unless you change your token.
Do not share this link.

Your email address should be: {{ email }}{% endblocktrans %}
{% endspaceless %}
