{% spaceless %}
{% load i18n %}
{% load global_tags %}

// @license [magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt] [GNU AGPL v3]
{% minify_include_js "js/functions.js" %}
{% if url_check_username %}{% minify_include_js "js/accounts_register.js" url_check_username %}{% endif %}
{% minify_include_js "js/parse.js" %}
{% endspaceless %}
// @license-end
