/* docCookies function made by Mozilla */
var docCookies = {
	getItem: function (sKey) {
		if (!sKey) { return null; }
		return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
	},
	setItem: function (sKey, sValue, vEnd, sPath, sDomain, bSecure) {
		if (!sKey || /^(?:expires|max\-age|path|domain|secure)$/i.test(sKey)) { return false; }
		var sExpires = "";
		if (vEnd) {
			switch (vEnd.constructor) {
				case Number:
					sExpires = vEnd === Infinity ? "; expires=Fri, 31 Dec 9999 23:59:59 GMT" : "; max-age=" + vEnd;
					break;
				case String:
					sExpires = "; expires=" + vEnd;
					break;
				case Date:
					sExpires = "; expires=" + vEnd.toUTCString();
					break;
			}
		}
		document.cookie = encodeURIComponent(sKey) + "=" + encodeURIComponent(sValue) + sExpires + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "") + (bSecure ? "; secure" : "");
		return true;
	},
	removeItem: function (sKey, sPath, sDomain) {
		if (!this.hasItem(sKey)) { return false; }
		document.cookie = encodeURIComponent(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "");
		return true;
	},
	hasItem: function (sKey) {
		if (!sKey) { return false; }
		return (new RegExp("(?:^|;\\s*)" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(document.cookie);
	},
	keys: function () {
		var aKeys = document.cookie.replace(/((?:^|\s*;)[^\=]+)(?=;|$)|^\s*|\s*(?:\=[^;]*)?(?:\1|$)/g, "").split(/\s*(?:\=[^;]*)?;\s*/);
		for (var nLen = aKeys.length, nIdx = 0; nIdx < nLen; nIdx++) { aKeys[nIdx] = decodeURIComponent(aKeys[nIdx]); }
		return aKeys;
	}
};

function save_feedback_to_cookie () {
	message    = document.getElementById('feedback_message').value;
	duplicate  = document.getElementById('feedback_duplicate_to_email').checked;
	email      = document.getElementById('feedback_email').value;
	channels   = document.getElementById('feedback_form').getElementsByClassName('channel');
	channel_id = "";
	for (i=0; i<channels.length; i++) {
		if (channels[i].checked) channel_id = channels[i].value;
	}
	json = '{"message": "' + message + '", "duplicate": "' + duplicate + '", "email": "' + email + '", "channel_id": "' + channel_id + '"}';
	docCookies.setItem('feedback', json, 31536e3, "/");
}

function restore_feedback_from_cookie () {
	try {
		feedback = JSON.parse(docCookies.getItem('feedback'));
	} catch (e) {
		docCookies.removeItem('feedback');
		feedback = null;
	}
	if (feedback) {
		document.getElementById('feedback_message').value = feedback.message;
		document.getElementById('feedback_duplicate_to_email').checked = (feedback.duplicate=="true");
		if (document.getElementById('feedback_email').value == '') document.getElementById('feedback_email').value = feedback.email;
		channels = document.getElementById('feedback_form').getElementsByClassName('channel');
		for (i=0; i<channels.length; i++) {
			if (channels[i].value == feedback.channel_id) channels[i].checked = true;
		}
	}
}

function toggle_account_active_class () {
	element = document.getElementById('id_account_form');
	class_name = 'active';
	if (!element.classList.contains(class_name)) {element.classList.add(class_name);}
	else {element.classList.remove(class_name);}
}

function toggle_comments_moderation (force_on) {
	mod_buttons = docCookies.getItem('mod_buttons');
	element = document.getElementById('toggle_comments_moderation');
	if (element) {
		if (!mod_buttons || force_on) {
			element.classList.add('button-success');
			docCookies.setItem('mod_buttons', '1', 31536e3, "/");
			sheet = document.styleSheets[0];
			window.comments_moderation_rule_id = sheet.insertRule('.comment .moderate {display: inline-block !important;}', sheet.cssRules.length);
		}
		else {
			element.classList.remove('button-success');
			docCookies.setItem('mod_buttons', '', 31536e3, "/");
			sheet = document.styleSheets[0];
			sheet.deleteRule(window.comments_moderation_rule_id);
		}
	}
}

function toggle_album_editor (force_on) {
	album_editor = docCookies.getItem('album_editor');
	if (!album_editor || force_on) {
		document.getElementById('toggle_album_editor').classList.add('button-success');
		docCookies.setItem('album_editor', '1', 31536e3, "/");
		sheet = document.styleSheets[0];
		window.album_editor_rule_id = sheet.insertRule('.gallery-thumbnail .album-editor {display: block !important;}', sheet.cssRules.length);
	}
	else {
		document.getElementById('toggle_album_editor').classList.remove('button-success');
		docCookies.setItem('album_editor', '', 31536e3, "/");
		sheet = document.styleSheets[0];
		sheet.deleteRule(window.album_editor_rule_id);
	}
}

function send_chat_message (event) {
	event.preventDefault();
	autoscroll.checked = true;
	data = 'message='+encodeURIComponent(event.target.message.value)+'&ajax=sucess';
	event.target.message.value = '';
	callAjax(document.location.pathname+ '/message', 'POST', message_callback, data);
}

function message_callback (response) {
	if (response == 'flood ban') {
		/* TODO localize this message */
		document.getElementById('chat_message_input').value = 'Autoban for 5 minutes';
		document.getElementById('chat_message_input').disabled = true;
		document.getElementById('chat_message_submit').disabled = true;
		/* TODO actual ban duration should be retrieved and used, also add an indicator somehow */
		setTimeout(function() {
			document.getElementById('chat_message_input').value = '';
			document.getElementById('chat_message_input').disabled = false;
			document.getElementById('chat_message_submit').disabled = false;
		}, 5*60*1000)
	} else {
		poll_chat();
	}
}

function chat_led (color) {
	chat_status_led.classList.remove('red', 'yellow', 'green');
	chat_status_led.classList.add(color);
}

function poll_chat (repoll=false) {
	chat_led_red = setTimeout( function() { chat_led('red'); }, 5000);
	messages = document.getElementById('chat_messages');
	uuid = messages.lastChild.dataset.uuid;
	callAjax(
		document.location.pathname+ '/poll?uuid=' + uuid,'GET',
		function(callback) { update_chat(callback, repoll); },
		null,
		function(callback) { if (repoll) {
			setTimeout(function() { poll_chat(repoll=true); }, 10000);
		}}
	);
}

function update_chat (response, repoll) {
	messages = document.getElementById('chat_messages');
	userlist = document.getElementById('chat_userlist');
	scrolled = (messages.scrollTop+messages.clientHeight >= messages.scrollHeight);
	uuid = messages.lastChild.dataset.uuid;
	data = JSON.parse(response);
	new_messages = data['messages'];
	user_list = data['userlist'];

	for (i in new_messages) {
		uuid       = new_messages[i][0];
		timestamp  = new_messages[i][1];
		username   = new_messages[i][2];
		text       = new_messages[i][3];
		if (!document.querySelector('[data-uuid="'+uuid+'"]')) {
			message = document.createElement('p');
			message.className = 'chat-message';
			message.dataset.uuid = uuid;
			message.innerHTML = timestamp+'&nbsp;'+username+'>&nbsp;'+text;
			messages.appendChild(message);
		}
	}

	for (nickname in user_list) {
		if (!document.querySelector('[data-nickname="'+nickname+'"]')) {
			user = document.createElement('div');
			user.dataset.nickname = nickname;
			user.classList.add('chat-user');
			user.innerHTML = '<img style="width: 32px; height: 32px;" src="'+user_list[nickname]['gravatar']+'"><span onclick="insert_chat_username(\''+nickname+'\');">'+nickname+'</span>';
			userlist.appendChild(user);
		}
	}

	userElements = document.getElementsByClassName('chat-user')
	for (i=0; i<userElements.length; i++) {
		if (!user_list[userElements[i].dataset.nickname]) {
			userElements[i].parentElement.removeChild(userElements[i]);
		}
	}

	if (autoscroll.checked) {
		messages.scrollTop = messages.scrollHeight-messages.clientHeight;
	}

/* TODO make something like this optional
	if (scrolled) {
		autoscroll.checked = true;
	}
*/

	if (repoll) {
		clearTimeout(chat_led_red);
		chat_led('green');
		setTimeout(function() { chat_led('yellow'); poll_chat(repoll=true); }, 2000);
	}

}

function insert_chat_username (username) {
	target = document.getElementById('chat_message_input');
	if (target.value.indexOf(username) == -1) {
		target.value += username;
		target.value += ', ';
	}
	target.focus();
}

function callAjax (url, method, callback, post, fallback) {
	var xmlhttp;
	post = post || '';
	xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4) {
			if (xmlhttp.status == 200) {
				callback(xmlhttp.responseText);
			} else {
				fallback(xmlhttp.responseText);
			}
		}
	}
	if (method == 'GET') {
		xmlhttp.open('GET', url, true);
		xmlhttp.send();
	}
	else if (method == 'POST') {
		xmlhttp.open("POST", url, true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.setRequestHeader("X-CSRFToken", docCookies.getItem('csrftoken'));
		xmlhttp.send(post);
	}
}

function insertAfter (newNode, referenceNode) {
	referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}
