{% load i18n %}
// @license [magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt] [GNU AGPL v3]
function adjust_textarea_height (textarea) {
	height_placeholder = document.getElementById('comment_form_gravatar');
	if (!parseInt(height_placeholder.style.height)) height_placeholder.style.height = '80px';
	if (parseInt(height_placeholder.style.height) < parseInt(textarea.style.height)) height_placeholder.style.height = textarea.style.height;
	textarea.style.height = '50px'
	height = 5+textarea.scrollHeight;
	if (height > 200) height = 200;
	textarea.style.height = height + 'px'
}

function activate_controls (controls) {
	for_deactivation = document.getElementsByClassName('active_controls');
	if (for_deactivation.length) {
		for (i=0; i<for_deactivation.length; i++) {
			for_deactivation[i].classList.remove('active_controls');
		}
	}
	if (controls.classList.contains('controls')) {
		controls.classList.add('active_controls');
	}
}

function move_comment_form (comment_uuid) {
	element = document.getElementById('comment_form');
	notice = document.getElementById('new_comment_notice');
	if (comment_uuid == '' || comment_uuid == 'hide' ) {
		if (notice) notice.style.display = 'block';
		destination = document.getElementById('id_comments');
		destination.appendChild(element);
	} else {
		if (notice) notice.style.display = 'none';
		destination = document.getElementById('comment_' + comment_uuid).getElementsByClassName('controls')[0];
		destination.parentElement.parentElement.parentElement.appendChild(element);
	}
	element.getElementsByClassName('parent_uuid')[0].value = comment_uuid;
	if (comment_uuid != 'hide' ) {
		element.classList.remove('hidden');
		activate_controls (destination);
		document.getElementById('id_content').focus();
	} else {
		element.classList.add('hidden');
	}
}

function edit_comment_form (comment_uuid, email_updates) {
	element = document.getElementById('edit_form');
	comment = document.getElementById('comment_' + comment_uuid);
	destination = document.getElementById('comment_content_' + comment_uuid);
	comment_body = destination.getElementsByTagName('pre')[0];
	comment_body.classList.add('hidden');
	destination_child = destination.getElementsByClassName('comment-content')[0]
	destination.insertBefore(element, destination_child);
	document.getElementById('id_edit_comment_uuid').value = comment_uuid;
	if (email_updates == 'True') document.getElementById('id_edit_email_updates').checked = true;
	element.classList.remove('hidden');
	activate_controls (comment.getElementsByClassName('controls')[0]);
	textarea = document.getElementById('id_edit_content');
	textarea.focus();
	textarea.value = '';
	textarea.value = comment_body.textContent;
	adjust_textarea_height(textarea);
}

function unfold (event) {
	alias = document.getElementById('id_comments_group_alias').value;
	comment_uuid = event.target.id.replace('unfold_', '').replace('fold_', '');
	fold_button = document.getElementById('fold_' + comment_uuid);
	fold_button.textContent = '...';
	fold_button.onclick = '';
	callAjax('{{ unfold_url }}', 'POST', branch_folded, 'uuid=' + comment_uuid + '&alias=' + alias);
}

function attach_branch (response) {
	uuid = response.match(/^<!-- attach me after [0123456789abcdef-]{36} -->/);
	response = response.replace(uuid, '')
	uuid = uuid[0].replace('<!-- attach me after ', '').replace(' -->', '');
	element = document.getElementById('comment_' + uuid);
	placeholder = document.createElement("div");
	insertAfter(placeholder, element);
	placeholder.outerHTML = response;
}

function cut_branch (comment_uuid) {
	move_comment_form('hide');
	comment = document.getElementById('comment_' + comment_uuid);
	comment = comment.nextSibling.nextSibling;
	while (comment.classList.contains('comment') && comment.dataset.level != 0) {
		next_comment = comment.nextSibling.nextSibling;
		placeholder = comment.previousSibling;
		placeholder.remove();
		comment.remove();
		comment = next_comment;
	}
}

function branch_folded (response) {
	comment_uuid = JSON.parse(response)['branch'];
	status = JSON.parse(response)['status'];
	fold_button = document.getElementById('fold_' + comment_uuid);
	if (status == 'folded') {
		fold_button.classList.remove('fold');
		fold_button.classList.add('unfold');
		fold_button.textContent = '+';
		cut_branch(comment_uuid);
	} else {
		fold_button.classList.remove('unfold');
		fold_button.classList.add('fold');
		fold_button.textContent = '-';
		alias = document.getElementById('id_comments_group_alias').value;
		show_deleted = (Q.show_deleted) ? ('&show_deleted=' + Q.show_deleted) : ''
		show_akismet_spam = (Q.show_akismet_spam) ? ('&show_akismet_spam=' + Q.show_akismet_spam) : ''
		show_manual_spam = (Q.show_manual_spam) ? ('&show_manual_spam=' + Q.show_manual_spam) : ''

		callAjax('{{ request_branch_url }}', 'POST', attach_branch, 'uuid=' + comment_uuid + '&alias=' + alias + show_deleted + show_akismet_spam + show_manual_spam);
	}
	fold_button.onclick = function onclick(event) {unfold(event);}
}

function comment_watch (comment_uuid, action) {
	alias = document.getElementById('id_comments_group_alias').value;
	watch_button = document.getElementById('watch_' + comment_uuid);
	watch_button.onclick = '';
	callAjax('{{ watch_url }}', 'POST', comment_watched, 'uuid=' + comment_uuid + '&alias=' + alias + '&action=' + action);
}

function group_watch (action) {
	alias = document.getElementById('id_comments_group_alias').value;
	watch_button = document.getElementById('group_watch');
	watch_button.textContent = '{% trans 'Working...' %}';
	watch_button.onclick = '';
	callAjax('{{ watch_group_url }}', 'POST', group_watched, 'alias=' + alias + '&action=' + action);
}

function comment_watched (response) {
	comment_uuid = JSON.parse(response)['uuid'];
	status = JSON.parse(response)['status'];
	watch_button = document.getElementById('watch_' + comment_uuid);
	if (status == 'watched') {
		watch_button.classList.remove('unwatched');
		watch_button.classList.add('watched');
		activate_unwatch_button(comment_uuid);
	}
	if (status == 'unwatched') {
		watch_button.classList.remove('watched');
		watch_button.classList.add('unwatched');
		activate_watch_button(comment_uuid);
	}
}

function group_watched (response) {
	status = JSON.parse(response)['status'];
	watch_button = document.getElementById('group_watch');
	if (status == 'watched') {
		watch_button.classList.remove('unwatched');
		watch_button.classList.add('watched');
		activate_group_unwatch_button();
	}
	if (status == 'unwatched') {
		watch_button.classList.remove('watched');
		watch_button.classList.add('unwatched');
		activate_group_watch_button();
	}
	console.log(response)
}

/* TODO decopypastify this */
function activate_watch_button (comment_uuid) {
	button = document.getElementById('watch_' + comment_uuid);
	button.onclick = function onclick(event) {comment_watch(comment_uuid, 'watch');}
}

function activate_unwatch_button (comment_uuid) {
	button = document.getElementById('watch_' + comment_uuid);
	button.onclick = function onclick(event) {comment_watch(comment_uuid, 'unwatch');}
}

function activate_group_watch_button () {
	watch_button.textContent = '{% trans 'Watch' %}';
	button = button = document.getElementById('group_watch');
	button.onclick = function onclick(event) {group_watch('watch');}
}

function activate_group_unwatch_button () {
	watch_button.textContent = '{% trans 'Unwatch' %}';
	button = button = document.getElementById('group_watch');
	button.onclick = function onclick(event) {group_watch('unwatch');}
}

{% if moderator %}
function moderate (comment_uuid, action) {
	alias = document.getElementById('id_comments_group_alias').value;
	mod_button = document.getElementById('moderate_' + comment_uuid);
	del_button = document.getElementById('delete_' + comment_uuid);
	spm_button = document.getElementById('spam_' + comment_uuid);
	if (action == 'approve' || action == 'unapprove') {mod_button.onclick = ''; mod_button.textContent = '{% trans 'Processing...' %}';}
	if (action == 'delete' || action == 'undelete') {del_button.onclick = ''; del_button.textContent = '{% trans 'Processing...' %}';}
	if (action == 'spam' || action == 'unspam') {spm_button.onclick = ''; spm_button.textContent = '{% trans 'Processing...' %}';}
	callAjax('{{ moderate_url }}', 'POST', moderated, 'uuid=' + comment_uuid + '&alias=' + alias + '&action=' + action);
}

function moderated (response) {
	comment_uuid = JSON.parse(response)['uuid'];
	status = JSON.parse(response)['status'];
	mod_button = document.getElementById('moderate_' + comment_uuid);
	del_button = document.getElementById('delete_' + comment_uuid);
	spm_button = document.getElementById('spam_' + comment_uuid);
	if (status == 'approved') {
		unapprove_button(comment_uuid);
		document.getElementById('comment_' + comment_uuid).classList.remove('unapproved');
	}
	if (status == 'unapproved') {
		approve_button(comment_uuid);
		document.getElementById('comment_' + comment_uuid).classList.add('unapproved');
	}
	if (status == 'parent unapproved') {
		mod_button.textContent = '{% trans 'Parent comment is not approved' %}';
		setTimeout("approve_button('" + comment_uuid + "');" , 2000);
	}
	if (status == 'have childs') {
		mod_button.textContent = '{% trans 'Comment already have childs' %}';
		setTimeout("unapprove_button('" + comment_uuid + "');" , 2000);
	}
	if (status == 'deleted') {
		undelete_button(comment_uuid);
		document.getElementById('comment_' + comment_uuid).classList.add('deleted');
		deleted_childs = JSON.parse(JSON.parse(response)['deleted_childs']);
		for (i in deleted_childs) {
			undelete_button(deleted_childs[i]);
			document.getElementById('comment_' + deleted_childs[i]).classList.add('deleted');
		}
	}
	if (status == 'undeleted') {
		delete_button(comment_uuid);
		document.getElementById('comment_' + comment_uuid).classList.remove('deleted');
	}
	if (status == 'parent deleted') {
		del_button.textContent = '{% trans 'Parent comment is deleted' %}';
		setTimeout("undelete_button('" + comment_uuid + "');" , 2000);
	}
	if (status == 'spam') {
		unspam_button(comment_uuid);
		document.getElementById('comment_' + comment_uuid).classList.add('manual_spam');
	}
	if (status == 'unspam') {
		spam_button(comment_uuid);
		document.getElementById('comment_' + comment_uuid).classList.remove('akismet_spam');
		document.getElementById('comment_' + comment_uuid).classList.remove('manual_spam');
	}
	if (status == 'error') {
		alert('{% trans 'Something is wrong' %}');
	}
	console.log(status);
}

function approve_button (comment_uuid) {
	button = document.getElementById('moderate_' + comment_uuid);
	button.onclick = function onclick(event) {moderate(comment_uuid, 'approve');}
	button.textContent = '{% trans 'Approve' %}';
}

function unapprove_button (comment_uuid) {
	button = document.getElementById('moderate_' + comment_uuid);
	button.onclick = function onclick(event) {moderate(comment_uuid, 'unapprove');}
	button.textContent = '{% trans 'Unapprove' %}';
}

function spam_button (comment_uuid) {
	button = document.getElementById('spam_' + comment_uuid);
	button.onclick = function onclick(event) {moderate(comment_uuid, 'spam');}
	button.textContent = '{% trans 'Spam' %}';
}

function unspam_button (comment_uuid) {
	button = document.getElementById('spam_' + comment_uuid);
	button.onclick = function onclick(event) {moderate(comment_uuid, 'unspam');}
	button.textContent = '{% trans 'Unspam' %}';
}

function delete_button (comment_uuid) {
	button = document.getElementById('delete_' + comment_uuid);
	button.onclick = function onclick(event) {moderate(comment_uuid, 'delete');}
	button.textContent = '{% trans 'Delete' %}';
}

function undelete_button (comment_uuid) {
	button = document.getElementById('delete_' + comment_uuid);
	button.onclick = function onclick(event) {moderate(comment_uuid, 'undelete');}
	button.textContent = '{% trans 'Undelete' %}';
}
{% endif %}
// @license-end
